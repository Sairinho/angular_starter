import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class UserService {
  constructor(private _httpClient: HttpClient) {}

  getUsers() {
    const url = environment.baseUrl + `/users`;
    return this._httpClient.get(url);
  }
}
